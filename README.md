# PRUEBA 

## LINK NOTEBOOK: https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/5715857033291365/2867746865438584/616964767462990/latest.html

## HERRAMIENTAS

se va a usar databricks y sus bases de datos por la facilidad deintegrar python, sql y la base de datos de databricks que internamente es hive

## ARQUITECTURA 
![alt text](image.png)

## CARGA DE ARCHIVOS EN BASE DE DATOS

se llevan los archivos al cluster de databricks se cargan como data frame y se escriben como tabla en formato delta, pensando a futuro se pueden llevar a s3 o azure storage y programar un trigger con la llegada que lance o ejecute nuestro proceso o conciliaciones en databricks

## TRANSFORMACIONES

se cambia formato fecha en en tablas en el esquema default y se pasa a gold, tambien se concatena la tarjeta

## TABLAS CREADAS 

### transacciones_clap : tabla original apartir del archivo

### transacciones_bansur : tabla original apartir del archivo

### gold.transacciones_clap :

```%sql 
create or replace table gold.transacciones_clap
As
select 
concat(INICIO6_TARJETA, FINAL4_TARJETA) as TARJETA,
TIPO_TRX,
MONTO,
to_timestamp(FECHA_TRANSACCION) as FECHA_TRANSACCION,
CODIGO_AUTORIZACION,
ID_BANCO,
to_timestamp(FECHA_RECEPCION_BANCO, 'yyyy-MM-dd') as FECHA_RECEPCION_BANCO
from transacciones_clap
```

### gold.conciliable_banksur :
```

%sql
create or replace table gold.transacciones_bansur
As
select 
TARJETA,
TIPO_TRX,
MONTO,
to_timestamp(cast(FECHA_TRANSACCION as string), 'yyyyMMdd') as FECHA_TRANSACCION,
CODIGO_AUTORIZACION,
ID_ADQUIRIENTE,
to_timestamp(FECHA_RECEPCION, 'yyyy-MM-dd') as FECHA_RECEPCION
from transacciones_bansur

```

### gold.conciliable_clap :

```

%sql
CREATE OR REPLACE TABLE gold.conciliable_banksur AS
WITH conciliable_banksur AS (
SELECT *,
         RANK() OVER (PARTITION BY ID_ADQUIRIENTE ORDER BY FECHA_TRANSACCION, TIPO_TRX ASC) AS rank
  FROM gold.transacciones_bansur)
SELECT * FROM conciliable_banksur
  WHERE 
  rank = 1 AND 
  TIPO_TRX != "CANCELACION" AND TIPO_TRX !="NO APLICA"

```

### gold.conciliable_banksur :

```
%sql
CREATE OR REPLACE TABLE gold.conciliable_clap AS
WITH conciliable_clap AS (
SELECT *,
         RANK() OVER (PARTITION BY ID_BANCO ORDER BY FECHA_TRANSACCION, TIPO_TRX ASC) AS rank
  FROM gold.transacciones_clap)
SELECT * FROM conciliable_clap
  WHERE 
  rank = 1 AND 
  TIPO_TRX != "CANCELACION" AND TIPO_TRX !="NO APLICA"

```

### gold.conciliable_totals :

```

%sql
CREATE OR REPLACE TABLE gold.conciliable_totals AS
select count(*) AS NRO_TR, sum(MONTO) AS TOTAL_MONTO, "CLAP" AS ENTIDAD from gold.conciliable_clap 
union all
select count(*) AS NRO_TR, sum(MONTO) AS TOTAL_MONTO, "BANKSUR" AS ENTIDAD from gold.conciliable_banksur 

```

### gold.conciliable_clap_bansur :

```

%sql
CREATE OR REPLACE TABLE gold.conciliable_clap_bansur AS
select 
case 
  when clap.TARJETA is null then bansur.TARJETA 
  else clap.TARJETA 
end As TARJETA,
case 
  when clap.TIPO_TRX is null then bansur.TIPO_TRX 
  else clap.TIPO_TRX 
end As TIPO_TRX,
case 
  when clap.MONTO is null then bansur.MONTO 
  else clap.MONTO 
end As MONTO,
case 
  when clap.FECHA_TRANSACCION is null then bansur.FECHA_TRANSACCION 
  else clap.FECHA_TRANSACCION 
end As FECHA_TRANSACCION,
case 
  when clap.CODIGO_AUTORIZACION is null then bansur.CODIGO_AUTORIZACION 
  else clap.CODIGO_AUTORIZACION 
end As CODIGO_AUTORIZACION,
case 
  when clap.ID_BANCO is null then bansur.ID_ADQUIRIENTE 
  else clap.ID_BANCO 
end As ID_BANCO,
case 
  when clap.FECHA_RECEPCION_BANCO is null then bansur.FECHA_RECEPCION
  else clap.FECHA_RECEPCION_BANCO 
end As FECHA_RECEPCION_BANCO,
case 
  when clap.rank is null then "BANKSUR" 
  when bansur.rank is null then "CLAP"
  else "AMBOS" 
end As ENTIDAD
from gold.conciliable_clap as clap
full outer join
gold.conciliable_banksur as bansur 
on bansur.ID_ADQUIRIENTE = clap.ID_BANCO and bansur.TARJETA = clap.TARJETA 
and (clap.MONTO = bansur.MONTO OR abs(clap.MONTO - bansur.MONTO) <= 0.99) --

and CAST(clap.FECHA_TRANSACCION AS DATE) = cast(bansur.FECHA_TRANSACCION AS DATE)


```

### gold.total_conciliable_clap_bansur :

```

%sql
CREATE OR REPLACE TABLE gold.total_conciliable_clap_bansur AS
WITH conciliable_clap_bansur AS (
select count(ENTIDAD) NRO_TR, sum(MONTO) as TOTAL_MONTO, ENTIDAD from gold.conciliable_clap_bansur group by ENTIDAD) 
SELECT clap_bansur.*, totals.NRO_TR as TOTAL_NRO_TR, totals.TOTAL_MONTO as TOTAL_TOTAL_MONTO
, (clap_bansur.NRO_TR*100/totals.NRO_TR) PORCENTAJE 
from conciliable_clap_bansur as clap_bansur LEFT JOIN gold.conciliable_totals as totals
on clap_bansur.ENTIDAD = totals.ENTIDAD

```


## PREGUNTAS Y RESPUESTAS 

1. Escriba el código de SQL que le permite conocer el monto y la cantidad de las transacciones que SIMETRIK 
considera como conciliables para la base de CLAP

```
%sql
CREATE OR REPLACE TABLE gold.conciliable_clap AS
WITH conciliable_clap AS (
SELECT *,
         RANK() OVER (PARTITION BY ID_BANCO ORDER BY FECHA_TRANSACCION, TIPO_TRX ASC) AS rank
  FROM gold.transacciones_clap)
SELECT * FROM conciliable_clap
  WHERE 
  rank = 1 AND 
  TIPO_TRX != "CANCELACION" AND TIPO_TRX !="NO APLICA"

```

2. Escriba el código de SQL que le permite conocer el monto y la cantidad de las transacciones que SIMETRIK considera 
como conciliables para la base de BANSUR

```

CREATE OR REPLACE TABLE gold.conciliable_banksur AS
WITH conciliable_banksur AS (
SELECT *,
         RANK() OVER (PARTITION BY ID_ADQUIRIENTE ORDER BY FECHA_TRANSACCION, TIPO_TRX ASC) AS rank
  FROM gold.transacciones_bansur)
SELECT * FROM conciliable_banksur
  WHERE 
  rank = 1 AND 
  TIPO_TRX != "CANCELACION" AND TIPO_TRX !="NO APLICA"

```

```
%sql
CREATE OR REPLACE TABLE gold.conciliable_totals AS
select count(*) AS NRO_TR, sum(MONTO) AS TOTAL_MONTO, "CLAP" AS ENTIDAD from gold.conciliable_clap 
union all
select count(*) AS NRO_TR, sum(MONTO) AS TOTAL_MONTO, "BANKSUR" AS ENTIDAD from gold.conciliable_banksur 

```

![alt text](image-1.png)


3. ¿Cómo se comparan las cifras de los puntos anteriores respecto de las cifras totales en las fuentes desde un punto de vista del negocio?

Existe una diferencia entre BANSUR y CLAP que se debe entrar a conciliar de 15067 y el monto aproximado de 7089641, esto pueden ser transacciones correctas que se generaron en clap pero por algun error de trasmision no se enviaron a BANSUR, se deben de validar por parte de BANSUR, por otra parte se debe de analizar en detalle que transacciones estan de BANSUR, en CLAP y en AMBAS para poder identificar las transacciones "perdidas" entre ambos sistemas y al final de este proceso definir cuales transacciones se reconocen por las partes y las que no generar las correspondientes devolucioneso generar notificaciones a los clientes

4. Teniendo en cuenta los criterios de cruce entre ambas bases conciliables, escriba una sentencia de SQL que contenga la información de CLAP y BANSUR; agregue una columna en la que se evidencie si la transacción cruzó o no con su contrapartida y una columna en la que se inserte un ID autoincremental para el control de la conciliación

```

%sql
CREATE OR REPLACE TABLE gold.conciliable_clap_bansur AS
select 
case 
  when clap.TARJETA is null then bansur.TARJETA 
  else clap.TARJETA 
end As TARJETA,
case 
  when clap.TIPO_TRX is null then bansur.TIPO_TRX 
  else clap.TIPO_TRX 
end As TIPO_TRX,
case 
  when clap.MONTO is null then bansur.MONTO 
  else clap.MONTO 
end As MONTO,
case 
  when clap.FECHA_TRANSACCION is null then bansur.FECHA_TRANSACCION 
  else clap.FECHA_TRANSACCION 
end As FECHA_TRANSACCION,
case 
  when clap.CODIGO_AUTORIZACION is null then bansur.CODIGO_AUTORIZACION 
  else clap.CODIGO_AUTORIZACION 
end As CODIGO_AUTORIZACION,
case 
  when clap.ID_BANCO is null then bansur.ID_ADQUIRIENTE 
  else clap.ID_BANCO 
end As ID_BANCO,
case 
  when clap.FECHA_RECEPCION_BANCO is null then bansur.FECHA_RECEPCION
  else clap.FECHA_RECEPCION_BANCO 
end As FECHA_RECEPCION_BANCO,
case 
  when clap.rank is null then "BANKSUR" 
  when bansur.rank is null then "CLAP"
  else "AMBOS" 
end As ENTIDAD
--, monotonically_increasing_id()
from gold.conciliable_clap as clap
full outer join
gold.conciliable_banksur as bansur 
on bansur.ID_ADQUIRIENTE = clap.ID_BANCO and bansur.TARJETA = clap.TARJETA 
and (clap.MONTO = bansur.MONTO OR abs(clap.MONTO - bansur.MONTO) <= 0.99) --

and CAST(clap.FECHA_TRANSACCION AS DATE) = cast(bansur.FECHA_TRANSACCION AS DATE)


```


```
%sql
CREATE OR REPLACE TABLE gold.conciliable_clap_bansurid AS
SELECT *,  monotonically_increasing_id() AS ID FROM gold.conciliable_clap_bansur 

```

5. Diseñe un código que calcule el porcentaje de transacciones de la base conciliable de CLAP cruzó contra la liquidación de BANSUR.

6. Diseñe un código que calcule el porcentaje de transacciones de la base conciliable de BANSUR no cruzó contra la liquidación de CLAP.

```

%sql
CREATE OR REPLACE TABLE gold.total_conciliable_clap_bansur AS
WITH conciliable_clap_bansur AS (
select count(ENTIDAD) NRO_TR, sum(MONTO) as TOTAL_MONTO, ENTIDAD from gold.conciliable_clap_bansur group by ENTIDAD) 
SELECT clap_bansur.*, totals.NRO_TR as TOTAL_NRO_TR, totals.TOTAL_MONTO as TOTAL_TOTAL_MONTO
, (clap_bansur.NRO_TR*100/totals.NRO_TR) PORCENTAJE 
from conciliable_clap_bansur as clap_bansur LEFT JOIN gold.conciliable_totals as totals
on clap_bansur.ENTIDAD = totals.ENTIDAD

```

![alt text](image-2.png)




